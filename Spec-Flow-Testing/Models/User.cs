﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Spec_Flow_Testing.Models
{
    public class User
    {
        [HiddenInput(DisplayValue = false)]
        public int UserId { get; set; }

        [DisplayName("First Name")]
        [Required(AllowEmptyStrings =false,ErrorMessage ="First Name is required")]
        [DataType(DataType.Text)]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Last Name is required")]
        [DataType(DataType.Text)]
        public string LastName { get; set; }

        [DisplayName("Contact Number")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Contact Number is required")]
        [Phone(ErrorMessage ="Please Enter a Valid Phone Number")]
        public string Contact { get; set; }

        [DisplayName("Email")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email is required")]
        [EmailAddress(ErrorMessage = "Please Enter a Valid Email Address")]
        public string Email { get; set; }
    }
}