﻿using System.Web;
using System.Web.Mvc;

namespace Spec_Flow_Testing
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
