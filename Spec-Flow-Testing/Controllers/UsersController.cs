﻿using Spec_Flow_Testing.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Spec_Flow_Testing.Controllers
{
    public class UsersController : Controller
    {
        // GET: Users
        public ActionResult Index()
        {
            var lst = new List<User>();
            if (Session["Data"] == null)
            {
                lst = new List<User>()   {
                new User{UserId=1,FirstName="Vishali",LastName="Reddy",Email="vishali.reddy@gmail.com",Contact="12345" }
                };
                Session["Data"] = lst;
            }
            else
                lst= (List<User>)Session["Data"];
            return View(lst);
        }
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(User user)
        {
            var lst = (List<User>)Session["Data"];
            var userId = lst.Max(s => s.UserId);
            user.UserId = userId + 1;
            lst.Add(user);
            return RedirectToAction(nameof(Index));
        }
    }
}